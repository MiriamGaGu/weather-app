import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor() {
    super()
    this.state = {
      text: "",

    }

  }
  addSomething = (a) => {
    alert("New city")
  }

  pressedKey = (e) => {
    e.preventDefault()
    const text = document.getElementsByClassName('app_input').value
    if (e.key === "Enter") {
      console.log('do validate');
      this.setState = ({ text })

    }


  }

  render() {
    return (
      <div className='app'>
        <header className='app__header'>
          <button onClick={this.addSomething} className='app__add'>
            <i className="fa-plus-circle" >New city</i>
            {/* /* Remove this comment and put an icon using `fa-plus-circle` class New city */}
          </button>
        </header>
        <div className='grid'>
          <aside className='app__aside'>
            <h1 className='app__title'>All countries</h1>
            <a href='#' className='app__country'>France</a>
            <input onKeyPress={this.pressedKey} type='text' placeholder='Location' className='app__input' />
          </aside>
          <section className='app__view'>Text</section>
        </div>
      </div>

    );
  }
}



export default App;
